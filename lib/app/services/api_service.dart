import 'dart:async';

import 'package:dio/dio.dart';

class ApiService {
  FutureOr getPosts() async {
    try {
      final client = Dio();
      final response = await client.get(
        'https://jsonplaceholder.typicode.com/posts',
      );
      return response;
    } on DioException catch (e) {
      return e.response;
    }
  }

  FutureOr getCommentsFromPosts(String postId) async {
    try {
      final client = Dio();
      final response = await client.get(
        'https://jsonplaceholder.typicode.com/comments?postId=$postId',
      );
      return response;
    } on DioException catch (e) {
      return e.response;
    }
  }
}
