class PostModel {
  final String? userId;
  final String? id;
  final String? title;
  final String? body;

  PostModel({this.userId, this.id, this.title, this.body});

  factory PostModel.fromJson(Map<String, dynamic> json) => PostModel(
        userId: json['userId'].toString(),
        id: json['id'].toString(),
        title: json['title'],
        body: json['body'],
      );
}
