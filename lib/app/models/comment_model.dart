class CommentModel {
  final String? postId;
  final String? id;
  final String? name;
  final String? email;
  final String? body;

  CommentModel({
    required this.postId,
    required this.id,
    required this.name,
    required this.email,
    required this.body,
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) => CommentModel(
        postId: json['postId'].toString(),
        id: json['id'].toString(),
        name: json['name'],
        email: json['email'],
        body: json['body'],
      );
}
