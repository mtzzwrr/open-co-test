import 'package:flutter_modular/flutter_modular.dart';
import 'package:open_test/app/blocs/comments/comments_bloc.dart';
import 'package:open_test/app/blocs/posts/posts_bloc.dart';
import 'package:open_test/app/screens/home_page.dart';
import 'package:open_test/app/screens/post_comments_page.dart';

class AppModule extends Module {
  @override
  void binds(i) {
    i.addSingleton<PostsBloc>(PostsBloc.new);
    i.addSingleton<CommentsBloc>(CommentsBloc.new);
  }

  @override
  void routes(r) {
    r.child(
      '/',
      child: (_) => const HomePage(),
      transition: TransitionType.fadeIn,
    );
    r.child(
      '/comments',
      child: (_) => PostCommentsPage(
        post: r.args.data,
      ),
      transition: TransitionType.fadeIn,
    );
  }
}
