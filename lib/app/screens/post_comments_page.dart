import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:open_test/app/blocs/comments/comments_bloc.dart';
import 'package:open_test/app/models/post_model.dart';

class PostCommentsPage extends StatelessWidget {
  final PostModel post;

  const PostCommentsPage({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    final commentsBloc = Modular.get<CommentsBloc>();
    commentsBloc.add(GetCommentsFromPostEvent(postId: post.id!));

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Modular.to.navigate('/');
          },
          icon: const Icon(
            Icons.arrow_back_ios,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(14.0),
        child: BlocBuilder<CommentsBloc, CommentsState>(
          bloc: commentsBloc,
          builder: (_, state) {
            switch (state.runtimeType) {
              case CommentsLoading:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case CommentsFailure:
                final errorMessage = (state as CommentsFailure).errorMessage;
                return Center(
                  child: Text(errorMessage),
                );
              case CommentsSuccess:
                final comments = (state as CommentsSuccess).comments;
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        constraints: const BoxConstraints(
                          minHeight: 140,
                        ),
                        margin: const EdgeInsets.only(bottom: 18),
                        padding: const EdgeInsets.all(8.0),
                        decoration: const BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              width: 1,
                              color: Color.fromARGB(
                                255,
                                216,
                                208,
                                208,
                              ),
                            ),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              post.title!,
                              style: GoogleFonts.play(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              post.body!,
                              style: GoogleFonts.play(
                                fontSize: 14,
                                color: const Color.fromARGB(255, 49, 48, 48),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            const SizedBox(height: 10),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            'Comentários',
                            style: GoogleFonts.play(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          const SizedBox(width: 10),
                          Container(
                            width: 28,
                            height: 28,
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(999),
                            ),
                            child: Center(
                              child: Text(
                                comments.length.toString(),
                                style: GoogleFonts.play(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: comments.length,
                          itemBuilder: (_, index) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              height: 140,
                              margin: const EdgeInsets.only(bottom: 18),
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      const Icon(Icons.person),
                                      Text(
                                        comments[index].email!,
                                        style: GoogleFonts.play(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    comments[index].body!,
                                    style: GoogleFonts.play(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
            }

            return const SizedBox();
          },
        ),
      ),
    );
  }
}
