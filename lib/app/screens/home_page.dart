import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:open_test/app/blocs/posts/posts_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final postsBloc = Modular.get<PostsBloc>();
    postsBloc.add(GetPostsEvent());

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Postagens',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.w800,
                ),
              ),
              const SizedBox(height: 14),
              Expanded(
                child: BlocBuilder<PostsBloc, PostsState>(
                    bloc: postsBloc,
                    builder: (_, state) {
                      switch (state.runtimeType) {
                        case PostsLoading:
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        case PostsSuccess:
                          final posts = (state as PostsSuccess).posts;
                          return Center(
                            child: ListView.builder(
                              itemCount: posts.length,
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Modular.to.navigate(
                                      '/comments',
                                      arguments: posts[index],
                                    );
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    constraints: const BoxConstraints(
                                      minHeight: 140,
                                    ),
                                    margin: const EdgeInsets.only(bottom: 18),
                                    padding: const EdgeInsets.all(8.0),
                                    decoration: const BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          width: 1,
                                          color: Color.fromARGB(
                                            255,
                                            216,
                                            208,
                                            208,
                                          ),
                                        ),
                                      ),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          posts[index].title!,
                                          style: GoogleFonts.play(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Text(
                                          posts[index].body!,
                                          style: GoogleFonts.play(
                                            fontSize: 14,
                                            color: const Color.fromARGB(
                                                255, 49, 48, 48),
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        const SizedBox(height: 8),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              'Ver comentários',
                                              style: GoogleFonts.sora(
                                                color: Colors.blue,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                      }
                      return const Column(
                        children: [
                          Text('oioioiassa'),
                        ],
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
