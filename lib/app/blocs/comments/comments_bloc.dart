import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:open_test/app/models/comment_model.dart';
import 'package:open_test/app/services/api_service.dart';

part 'comments_event.dart';
part 'comments_state.dart';

class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {
  CommentsBloc() : super(CommentsInitial()) {
    on<GetCommentsFromPostEvent>(getCommentsFromPostEvent);
  }

  FutureOr<void> getCommentsFromPostEvent(event, emit) async {
    try {
      emit(CommentsLoading());
      ApiService api = ApiService();
      final response = await api.getCommentsFromPosts(event.postId);
      if (response.statusCode == 200) {
        List<CommentModel> comments = [];
        for (final comment in response.data) {
          comments = comments..add(CommentModel.fromJson(comment));
        }
        emit(CommentsSuccess(comments: comments));
      }
    } catch (e) {
      emit(
        CommentsFailure(
          errorMessage: 'Não foi possível obter a lista de posts',
        ),
      );
    }
  }
}
