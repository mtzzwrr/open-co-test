part of 'comments_bloc.dart';

abstract class CommentsEvent extends Equatable {}

final class GetCommentsFromPostEvent extends CommentsEvent {
  final String postId;

  GetCommentsFromPostEvent({required this.postId});

  @override
  List<Object?> get props => [postId];
}
