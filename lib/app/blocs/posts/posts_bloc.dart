import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:open_test/app/models/post_model.dart';
import 'package:open_test/app/services/api_service.dart';

part 'posts_event.dart';
part 'posts_state.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  PostsBloc() : super(PostsInitial()) {
    on<GetPostsEvent>(getPostsEvent);
  }

  FutureOr<void> getPostsEvent(event, emit) async {
    try {
      emit(PostsLoading());
      ApiService api = ApiService();
      final response = await api.getPosts();
      if (response.statusCode == 200) {
        List<PostModel> posts = [];
        for (final post in response.data) {
          posts = posts..add(PostModel.fromJson(post));
        }
        emit(PostsSuccess(posts: posts));
      }
    } catch (e) {
      emit(
        PostsFailure(errorMessage: 'Não foi possível obter a lista de posts'),
      );
    }
  }
}
