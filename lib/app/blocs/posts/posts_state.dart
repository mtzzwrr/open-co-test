part of 'posts_bloc.dart';

abstract class PostsState extends Equatable {
  @override
  List<Object?> get props => [];
}

final class PostsInitial extends PostsState {}

final class PostsLoading extends PostsState {}

final class PostsSuccess extends PostsState {
  final List<PostModel> posts;

  PostsSuccess({
    required this.posts,
  });
}

final class PostsFailure extends PostsState {
  final String errorMessage;

  PostsFailure({
    required this.errorMessage,
  });
}
